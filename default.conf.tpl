server {
    listen ${LISTEN-PORT};
    
    location /static {
        alias /vol/static/;
    }

    location / {
        uwsgi-pass               ${APP_HOST}:{APP_PORT};
        include                  /etc/nginx/uwsgi_params;
        client_max_body_size     10M;
    }
}

